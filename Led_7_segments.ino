const int APin = 6;
const int BPin = 5;
const int CPin = 4;
const int DPin = 11;
const int EPin = 10;
const int FPin = 8;
const int GPin = 9;
const int DPPin = 3;

const int time = 4000;

void setup() {
  // put your setup code here, to run once:
  pinModeAll();
}

void loop() {  
  lightSegment(APin);
  
  lightSegment(BPin);

  lightSegment(CPin);

  lightSegment(DPin);

  lightSegment(EPin);
  
  lightSegment(FPin);

  lightSegment(GPin);

  lightSegment(DPPin);
}

void lightSegment(int segment)
{
  digitalWrite(segment,HIGH);
  delay(time);
}


void pinModeAll(){
  pinMode(APin, OUTPUT);
  pinMode(BPin, OUTPUT);
  pinMode(CPin, OUTPUT);
  pinMode(DPin, OUTPUT);
  pinMode(EPin, OUTPUT);
  pinMode(FPin, OUTPUT);
  pinMode(GPin, OUTPUT);
  pinMode(DPPin, OUTPUT);
}
